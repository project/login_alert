CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION:
-----------
Login Alert module is very useful to check if anyone login to the site.
This module provide a field on user profile page which allow user to get
notification once they logged in to the site.

If user choose option to receive notification than once user will login then
that user will receive an email notification to there registered email with
a link to logout.

Once user will click on that link then all session of that user will be
destroy and that user will be logout from all browsers.

This module is very useful for the security purpose. Anytime if user login
than login user will receive notification on there email address and 
they can secure there account.


REQUIREMENTS:
-----------
Drupal 8.x

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
-------------

This module will create an admin configuration page:
admin/config/people/login-alert.

MAINTAINERS
-----------

Current maintainers:
 * Gaurav Jhaloya (Gaurav_Jhaloya) - https://www.drupal.org/u/gaurav_jhaloya
 * Arvind Verma (arvind_verma) - https://www.drupal.org/u/arvind_verma
